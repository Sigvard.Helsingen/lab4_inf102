package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {
    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int length = list.size();
        int low = 0;
        int high = length-1;
        int k = (length/2)+1;
        return findK(listCopy, low, high, k);
        }
        
    public <T extends Comparable<T>> T findK (List<T> list, int low, int high, int k){
        int partition = partition(list, low, high);

        if (partition == k-1){
            return list.get(partition);
        }
        else if (partition < k-1){
            return findK(list, partition+1, high, k);
        }
        else {
            return findK(list, low, partition-1, k);
        }
    }
    public static <T extends Comparable<T>> int partition (List<T> list, int low, int high){
        T pivot = list.get(high);
        int pivotIndex = low;

        for (int i = low; i < high; i++){
            if (list.get(i).compareTo(pivot)<0){
                T temp = list.get(i);
                list.set(i, list.get(pivotIndex));
                list.set(pivotIndex, temp);
                pivotIndex++;
            }
        }
        T temp = list.get(high);
        list.set(high, list.get(pivotIndex));
        list.set(pivotIndex, temp);
        return pivotIndex;
    }
}
