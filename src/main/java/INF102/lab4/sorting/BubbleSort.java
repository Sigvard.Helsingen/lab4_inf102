package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int listLength = list.size();
        T myNum; 
        int i;
        int j;
        boolean isSwapped;
        for (i = 0; i < listLength-1; i++){
            isSwapped = false;
            for (j = 0; j < listLength-i-1; j++){
                if (list.get(j).compareTo(list.get(j+1)) > 0){
                    myNum = list.get(j);
                    list.set(j, list.get(j+1));
                    list.set(j+1, myNum);
                    isSwapped = true;
                }
            }
            if (isSwapped == false){
                break;
            }
        }
    }
}
